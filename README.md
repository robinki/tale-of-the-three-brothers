# Harry Potter and the Deathly Hollows

## CHAPTER TWENTY-ONE
## The Tale of the Three Brothers

Repository to store my work on the first page of chapter twenty-one of 
"Harry Potter and the Deathly Hollows".

I don't have the book in English, but I tried to resemble the look as close
as possible.


See the pdf file in the src/ folder for the final page!


I included the font file for Adobe Garamond, which is as close as I could get to the original font. Install it as noted in the corresponding 
readme file.

The goal is to craft something like this:
https://www.etsystudio.com/nz/listing/482787158/harry-potter-framed-deathly-hallows